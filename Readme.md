# Europass Pdf to Json

Simple wrapper around the Europass API to extract metadata from a Pdf CV.

## Usage

Install the package with:

```shell
npm install europass-pdf-to-json

# or

yarn add europass-pdf-to-json
```

then you can import it:

```javascript
import { convertPdf } from "europass-pdf-to-json";
```

The function `convertPdf` takes the path to a Europass CV Pdf file and returns a `Promise` that resolve to an object containing the CV metadata.

If you are using `fp-ts` and prefer a functional interface you can use:

```javascript
import { convertPdf } from "europass-pdf-to-json/fp";
```

This version of `convertPdf` returns a `TaskEither`.

## License

This software is released under the MIT license
