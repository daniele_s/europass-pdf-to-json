import fetch from "node-fetch";
import FormData from "form-data";
import fs from "fs";
import { pipe } from "fp-ts/function";
import * as IO from "fp-ts/IO";
import * as TE from "fp-ts/TaskEither";

function openFileStream(pdfPath: string): IO.IO<fs.ReadStream> {
  return IO.of(fs.createReadStream(pdfPath));
}

function createFormData(file: fs.ReadStream): FormData {
  const formData = new FormData();
  formData.append("file", file);
  return formData;
}

function convert(formData: FormData): TE.TaskEither<string, unknown> {
  return pipe(
    TE.tryCatch(
      async () => {
        const request = await fetch(
          "https://europa.eu/europass/eportfolio/api/eprofile/europass-cv",
          {
            method: "POST",
            body: formData,
            headers: {
              "X-XSRF-TOKEN": "dummy",
              Cookie: "XSRF-TOKEN=dummy",
            },
          }
        );

        return request.json();
      },
      () => "An error occurred"
    )
  );
}

export function convertPdf(pdfPath: string): TE.TaskEither<string, unknown> {
  return pipe(
    pdfPath,
    openFileStream,
    IO.map(createFormData),
    TE.fromIO,
    TE.chain(convert)
  );
}
