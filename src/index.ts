import * as TE from "fp-ts/TaskEither";
import { convertPdf as convertPdfFp } from "./fp";
import { pipe } from "fp-ts/function";

export const convertPdf: (filePath: string) => Promise<unknown> = (file) =>
  pipe(
    file,
    convertPdfFp,
    TE.getOrElseW((error) => {
      throw error;
    })
  )();
